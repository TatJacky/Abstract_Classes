public abstract class VegetarianSandwich implements Sandwich{
    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        if(this.filling == ""){
            this.filling += topping + " ";
        }
        else{
            this.filling += ", " + topping + " ";
        }
    }
}