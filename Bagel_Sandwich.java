public class Bagel_Sandwich implements Sandwich {
    private String filling;

    public Bagel_Sandwich(){
        this.filling = "";
    }

    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        if(this.filling == ""){
            this.filling += topping + " ";
        }
        else{
            this.filling += ", " + topping + " ";
        }
    }

    public boolean isVegetarian(){
        throw new UnsupportedOperationException("hehexd");
    }
}
